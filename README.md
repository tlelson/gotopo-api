# GOTOPO API

## Aim:
To become familiar with AWS Lambda and the [serverless](serverless.com) managment CLI framework.

## Result:
Python code I originally wrote to generate topographic maps for compass navigation, is now available through a scalable API hosted in AWS and running from AWS Lambda.

## Deployment - Production

When dev is ready to be promoted:

### Step 1 - Remove old API
```
$ sls project remove
```

manually remove lambdas and API from Console


### Step 2 - Update Master brance
- remove `_meta` and commit
- Submit a Pull Request to `master` (or just do a merge so you can fix the conflicts in `master`)
- Approve the pull request
- In dev reset the commit that remove `_meta` to get it back

### Step 3 - Create new API

```
$ sls project init
```
add the braintree variables to a variable file

```
$ sls dash deploy
```

### Step 4 - Update API doc
The chrome extension queries a file at gotopo.co for the API address. Use `update_apiaddr.py` to update the production document

### Step 5 - Update Repo
Once you have verified it works with the chrome extension. Commit the code and tag

## Deployment - Dev

### Create Cloud resources
```bash
$ sls project init
```

### Set braintree keys
In one of variable files add the following config to allow the lambda functions to be deployed with the required braintree payment keys.

Environments may be either `Sandbox` or `Production`

```
{
  "btree_env": "Sandbox",
  "btree_merchant_id": "cjxrntyjxqpskxgy",
  "btree_public_key": "jvj4cfv86yqgcwp2",
  "btree_private_key": "..."
}
```

### Deploy the API and lambda
```bash
$ sls dash deploy
```

## Development
The python packages that are used must be deployed in a single zip.  To do this we create
two conda environments, the first to simulate the conditions in AWS lambda, the second to
hold all the dependencies.

We run and test locally in the aws environment, the reqirements must be manually added to
the package (see the directory 'site-packages') and this directory must be added to each lambda function script.

### Setup Development Environment

Create 2 Conda environments

1. aws

This replicates the environment that the code runs in on AWS lambda

- python2.7
- boto3
- nodejs

    - npm install serverless -g

!! Nothing else.

Everything else must be in the site-packages locally as below

2. gotopo-api

This manages packages in the local 'site-packages' directory in functions/

```bash
$ conda create -n gotopo-api --file conda-site-packages.list        # Create a conda environment with the atached package list
$ activate gotopo-api
$ conda install <package>                                           # Manually Install the package not in conda (pip-site-packages.list)
```

3. Odd packages to cherry pick

I'm still wondering how to do this properly but these packages are just symlinked to from some packages.

To setup:
```
cat pip_packages.txt | xargs pip install -t  packs_to_cherrypick/
```

To update:
```
$ ls packs_to_cherrypick | grep -v info > pip_packages.txt
```

### Using these packages
To use these packages in the 'aws' conda environment soft link into the functions directory

```bash
$ ln -s /Users/home/miniconda3/envs/gotopo-api/lib/python2.7/site-packages site-packages
```

Then add the directory to sys.path in code

```python
# Import local packages
import sys
import os
here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "..", "site-packages"))
```

**N.B! broken symlinks can cause ambiguous errors when deploying functions:**

```bash
(aws) [minmac@minmac chromeauthorizer]# sls function deploy
Serverless: Deploying the specified functions in "dev" to the following regions: ap-northeast-1
Serverless: ------------------------
Serverless: Failed to deploy the following functions in "dev" to the following regions:
Serverless: ap-northeast-1 ------------------------
Serverless:   chromeauthorizer: undefined
/Users/minmac/miniconda3/envs/aws/lib/node_modules/serverless/node_modules/bluebird/js/release/async.js:61
        fn = function () { throw arg; };
                           ^

ServerlessError: Function Deployment Failed
    at new ServerlessError (/Users/minmac/miniconda3/envs/aws/lib/node_modules/serverless/lib/Error.js:17:11)
    at FunctionDeploy.<anonymous> (/Users/minmac/miniconda3/envs/aws/lib/node_modules/serverless/lib/actions/FunctionDeploy.js:126:19)
    at next (/Users/minmac/miniconda3/envs/aws/lib/node_modules/serverless/node_modules/fs-extra/node_modules/rimraf/rimraf.js:74:7)
    at FSReqWrap.CB [as oncomplete] (/Users/minmac/miniconda3/envs/aws/lib/node_modules/serverless/node_modules/fs-extra/node_modules/rimraf/rimraf.js:11
```

### Tracking requirements

Save the list of installed packages to the package.list files:

**DONT DO THIS ANYMORE:** The current list is cross platform (linux-64/osx-64) so leave it the way it is.  If you need to update the tmapper version, *do it maually.*

```bash
$ conda list -n gotopo-api -e > conda-site-packages.list
$ conda list -n gotopo-api | grep '<pip>' > pip-site-packages.list
```

Or, run the script which does this:
```bash
$ bash save-dependencies.sh
```

### Testing lambdas locally
Use the `(aws)` environment to simulate the lambda envrionment.

One important difference is in the avaiable shared libraries.  Because lambda only allows small zip files (<50MB>) you have to carefully choose which dynamically linked libraries you add.

Unfortunately it seems to be impossible to append to the `LD_LIBRARY_PATH` using serverless (Issue 1763)[https://github.com/serverless/serverless/issues/1763].

As such, code is required to symlink the required libraries to `/var/task/lib`.  This directory is already on the `LD_LIBRARY_PATH` in AWS lambda.  To simulate this in the local environment you should:
1. Set your `LD_LIBRARY_PATH` so that the libraries are not found elsewhere
2. Run `map_mapper` from the `gotopo` directory. (in AWS lambda code is run from `/var/task` which contains the `functions` dir)

### Unit Testing

There are some crud unit tests implimented in `tests/`.

`test_addcredit.py` - Will test local code with braintree test nonces.  Does not require code to be deployed as an API.  Only requires braintree sandbox keys
`test_endpoints.py` - Requires API to be deployed and a valid chrome auth token added to the test file.  Actually alot faster than `test_addcredit.py` because the webrequests are all don Asyncronously.

```
$ py.test test_endpoints.py
```

## Tags:
### 0.3 - Api returning link to Topo

#### Non-ticketed (Issued in Bitbucket) Items
- Enable Issue tracking in Bitbucket
- Add tmapper module
- Create dev branch
- New authoriser (chromeauthorizer)
- Create 3 new API endpoints

  - GET credit from GoTopo-User DB
  - UPDATE credit from GoTopo-User DB
  - POST map request:

#### ... Other additions tracked in Issues ...

### 0.2 - All Endpoints reposponding
#### Credit Operations
GET, DELETE and 2 PATCH methods are implimented on the GoTopo-guser database to return a users map credits and update them. None are authorised because they're all essentially temporary until a payment system works.

The *GoTopo-guser* table is V2 of GoTopo-User.  Rather than using the the email address we use the google id as the unique key.  This is possible to pass in a URL. The old table is kept for now until the first authoriser *authorizer1* is refactored to use it.

##### Usage

*Credit function*

To get the users map_credits, you need the google id.  This operation is not authorized:
```bash
$ curl https://1l792ikdf5.execute-api.ap-northeast-1.amazonaws.com/dev/credit/105899983401737132958
"13"
```

To increase a users map credits pass data to the patch. No auth or verification is done because this is temporary only until Paypal can be used

```bash
curl -X PATCH -i https://1l792ikdf5.execute-api.ap-northeast-1.amazonaws.com/dev/addcredit/112535796705570131483/2 -H "Content-Type: application/json" --data "{"email":"Jojo2@gmail.com"}"
{"map_credits": 2}
```
... there is a `setcredit` method (PATCH) that is used in the same way

To delete a user from the database use:
```bash
$ curl -X DELETE https://1l792ikdf5.execute-api.ap-northeast-1.amazonaws.com/dev/credit/105899983401737132958
```

*Map function*

No business logic implimented yet.  But a call to this endpoint is authorised with the users chromeauthtoken

```bash
$ curl https://1l792ikdf5.execute-api.ap-northeast-1.amazonaws.com/dev/map/1aK_gOJbe4XYxlBoQnwkkPja3WOs -H 'chromeauthtoken: ya29.CjY...'
{"status": 0, "message": null, "mid": null, "download_url": "http://gotopo.co/maps/ai89c03mf4429/AndohahelaNationalPark.gif"}
```

### 0.1 - First working Google Authenticator using the ID token (not available from chrome)
Success:
```bash
$ curl https://kqsq2gg711.execute-api.ap-northeast-1.amazonaws.com/dev/test -H 'googleidtoken: aXdfasTf...'
{"Heres your map!!": {}}
```
* Turns out you can replace the last letter of the idtoken with any alphabetically after it and it will still work!!*

Bad token:
```
{"message":null}
```
No token (no header):
```
{"message":"Unauthorized"}
```

#### To Get Authtokens:
- Use the `google_auth.html` page found in `frontend-demos/Google-Auth` to get valid current goole auth tokens.
- Run through a webserver `$ python -m http.server 8002` in the directory
- Copy and paste these into the even.json for the authoriser

#### Done
- Add GoTopo-Auth table to cloudformation template
- Make a dummy GET endpoint to test authentication on
- Hook up auth in the functions json spec sheets
- Save the test CURL command here

* Getting {"message":null} from an apparently authenticated response~~

