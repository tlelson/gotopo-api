from __future__ import print_function

#import simplejson as json
import os
import sys
import json
import unittest2 as unittest
#import grequests as gr

os.environ["SERVERLESS_STAGE"] = "dev"

# Braintree sandbox numbers
os.environ["btree_merchant_id"] = "cjxrntyjxqpskxgy"
os.environ["btree_public_key"] = "jvj4cfv86yqgcwp2"
os.environ["btree_private_key"] = "8ff351c96193cf6badf3ccc9d07f92aa"

here = os.path.dirname(os.path.realpath(__file__))
credit_pkgs = os.path.join(here, "..", "gotopo", "functions", "credit")
sys.path.append(credit_pkgs)

import handler as credit
import braintree

print('Braintree type: {}',format(type(braintree)))

braintree.Configuration.configure(braintree.Environment.Sandbox,
                merchant_id = os.environ['btree_merchant_id'],
                public_key =  os.environ['btree_public_key'],
                private_key = os.environ['btree_private_key'])


def request_failed(request, exception):
    print(request)
    print(exception)
    print('failed')

valid_nonces = [
    "fake-valid-nonce",
    "fake-valid-no-billing-address-nonce",
    "fake-valid-visa-nonce",
    "fake-valid-amex-nonce",
    "fake-valid-mastercard-nonce",
    "fake-valid-discover-nonce",
    "fake-valid-jcb-nonce",
#   "fake-valid-maestro-nonce",    # Not accepted, CVV maybe??
    "fake-valid-dinersclub-nonce",
    "fake-valid-prepaid-nonce",
    "fake-valid-commercial-nonce",
    "fake-valid-durbin-regulated-nonce",
    "fake-valid-healthcare-nonce",
    "fake-valid-debit-nonce",
    "fake-valid-payroll-nonce"
]

processor_rejected_nonces = [
    "fake-processor-declined-visa-nonce",
    "fake-processor-declined-mastercard-nonce",
    "fake-processor-declined-amex-nonce",
    "fake-processor-declined-discover-nonce",
    "fake-processor-failure-jcb-nonce"
]

gateway_rejected_nonces = [
    "fake-luhn-invalid-nonce",
    "fake-consumed-nonce",
#    "fake-gateway-rejected-fraud-nonce"   # Getts through!!!
]

generic_event = {
    "credits": "1",
    "authorizerPrincipalId": "1125",
    "payment_nonce": "fake-valid-visa-nonce",
    "resourcePath": "/addcredit",
    "httpMethod": "POST"
}

def deleteUser():
    try:
        cust_id = braintree.Customer.find(generic_event['authorizerPrincipalId'])
        result = braintree.Customer.delete(generic_event['authorizerPrincipalId'])
    except braintree.exceptions.not_found_error.NotFoundError as e:
        #print('Exception reached: ' + e.message)
        # No need to delete the user
        pass

    delete_event = generic_event.copy()
    delete_event['httpMethod'] = 'DELETE'

    res = credit.handler(delete_event, None)


class Test_Valid_Nonces(unittest.TestCase):

    def tearDown(self):
        deleteUser()

    def setUp(self):
        deleteUser()

    def test_good_nonce(self):
        for i, nonce in enumerate(valid_nonces):
            event = generic_event.copy()
            event['payment_nonce'] = nonce

            res = credit.handler(event, None)

            target_credits = 1 + i
            self.assertDictEqual({'map_credits': target_credits}, res,
                    'Expected {} map_credits. Got {}. Nonce: {}'.format(target_credits, res['map_credits'], nonce))

class Test_InValid_Nonces(unittest.TestCase):

    def setUp(self):
        deleteUser()

    def tearDown(self):
        deleteUser()

    def test_processor_rejected_nonces(self):
        for i, nonce in enumerate(processor_rejected_nonces):
            event = generic_event.copy()
            event['payment_nonce'] = nonce

            msg = "Nonce '{}' failed to raise an exception".format(nonce)
            #with self.assertRaises(Exception):
            #    res = credit.handler(event, None)
            try:
                res = credit.handler(event, None)
            except credit.MyException as e:
                pass  # intended
            else:
                raise Exception(msg)

    def test_gateway_rejected_nonces(self):
        for i, nonce in enumerate(gateway_rejected_nonces):
            event = generic_event.copy()
            event['payment_nonce'] = nonce

            msg = "Nonce '{}' failed to raise an exception".format(nonce)
            #with self.assertRaises(Exception):
            #    res = credit.handler(event, None)
            try:
                res = credit.handler(event, None)
            except credit.MyException as e:
                pass  # intended
            else:
                raise Exception(msg)



