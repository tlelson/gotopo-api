# How To Test

## Remove use and map_credits
The braintree sandbox limits payment addresses to 50. So remove the whole user before starting.  `test_addcredit.py` does this automatically but no gotopoAPI endpoint is exposed for this so it has to be done manually.

```
$ python remove_bt_client.py
```

## API Endpoint test

1. Update the API address and chrometoken
2. Run the tests

```
$ py.test test_endpoints.py::Test_Valid_Nonces
```

### Curl equivilent
Save the `TOKEN` and `APIADDR` as bash variables first. Then:

```
$ curl -i -XPOST -H "chromeauthtoken: $TOKEN" -H "Content-Type: application/json" --data "{"credit":"1", "payment_method_nonce": "fake-valid-nonce"}" ${APIADDR}/addcredit
```

## Test the function responses

```
$ py.test test_addcredit.py
```

