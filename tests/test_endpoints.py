from __future__ import print_function

#import simplejson as json
import json
import unittest
import grequests as gr

APIADDR = "https://ecndr76uo9.execute-api.ap-northeast-1.amazonaws.com/dev/"
CHROME_TOKEN = "ya29.CjBgA5LXimfpMBzlkwlymXgEF8y2tLmexQvA0QsIOUIpsriPwZX_ViUZTMiXvr7yWdU"
#GOOD_MID = "1vnelc0ZWiTvDd07ROsXlMhCoWw4"

valid_nonces = [
    "fake-valid-no-billing-address-nonce",
    "fake-valid-visa-nonce",
    "fake-valid-amex-nonce",
    "fake-valid-nonce",
    "fake-valid-mastercard-nonce",
    "fake-valid-discover-nonce",
    "fake-valid-jcb-nonce",
    #"fake-valid-maestro-nonce",    # Not accepting maestro
    "fake-valid-dinersclub-nonce",
    "fake-valid-prepaid-nonce",
    "fake-valid-commercial-nonce",
    "fake-valid-durbin-regulated-nonce",
    "fake-valid-healthcare-nonce",
    "fake-valid-debit-nonce",
    "fake-valid-payroll-nonce"
]

my_valid_nonces = [
]

processor_rejected_nonces = [
    "fake-processor-declined-visa-nonce",
    "fake-processor-declined-mastercard-nonce",
    "fake-processor-declined-amex-nonce",
    "fake-processor-declined-discover-nonce",
    "fake-processor-failure-jcb-nonce"
]

gateway_rejected_nonces = [
    "fake-luhn-invalid-nonce",
    "fake-consumed-nonce",
    "fake-gateway-rejected-fraud-nonce"   # Used to get through but doesnt now
]

my_invalid_nonces = [
    "fake-valid-maestro-nonce",    # Not accepted, CVV maybe??
]

def request_failed(request, exception):
    print(request)
    print(exception)
    print('failed')
    raise Exception('Failed: {}: {}'.format(request, exception))

class Test_Endpoints(unittest.TestCase):
    def test_200_from_all(self):
        headers = {
            'chromeauthtoken': CHROME_TOKEN,
            'Content-Type': "application/json"
        }
        post_data = {
            "credit": "15",
            "payment_method_nonce": "fake-valid-nonce"
        }

        rs = [
            #gr.get(APIADDR + 'map/' + GOOD_MID, headers=headers),  # Takes too long
            gr.get(APIADDR + 'ping'),
            gr.post(APIADDR + 'addcredit', headers=headers, data=json.dumps(post_data)),
            gr.get(APIADDR + 'credit', headers=headers),
            gr.delete(APIADDR + 'credit', headers=headers)
        ]
        responses = gr.map(rs, exception_handler=request_failed)
        for r in responses:
            self.assertEqual(r.status_code, 200, msg='Endpoint failed to return success: ' + r.url)

    def test_bad_token(self):
        headers = {'chromeauthtoken': CHROME_TOKEN + "XXX"}

        rs = [
            gr.get(APIADDR + 'credit', headers=headers),
            gr.delete(APIADDR + 'credit', headers=headers)
        ]
        responses = gr.map(rs, exception_handler=request_failed)
        for r in responses:
            self.assertEqual(r.status_code, 500, msg='Incorrect response to invalid token: ' + r.url)
        # r.reason == 'Internal Server Error'

    def test_no_token(self):
        headers = {
            'Content-Type': "application/json"
        }
        rs = [
            gr.get(APIADDR + 'credit', headers=headers),
            gr.delete(APIADDR + 'credit', headers=headers)
        ]
        responses = gr.map(rs, exception_handler=request_failed)
        for r in responses:
            self.assertEqual(r.status_code, 401, msg='Incorrect response to invalid token: {} : {}'.format(r.url, r.reason))

class MyTestCase(unittest.TestCase):
    def assert_responses_not(self, target_code, responses):
        for r in responses:
            cur_nonce = json.loads(r.request.body)['payment_method_nonce']
            self.assertNotEqual(r.status_code, target_code, msg='Endpoint responded with: {} for nonce: {}: Content: {}'.format(r.status_code, cur_nonce, r.content))

    def assert_responses_are(self, target_code, responses):
        for r in responses:
            cur_nonce = json.loads(r.request.body)['payment_method_nonce']
            self.assertEqual(r.status_code, target_code, msg='Endpoint responded with: {} for nonce: {}: Content: {}'.format(r.status_code, cur_nonce, r.content))

    def success_nonce_test(self, nonces):
        if not nonces: return

        headers = {
            'chromeauthtoken': CHROME_TOKEN,
            'Content-Type': "application/json"
        }
        post_data = {
            "credit": "1",
            "payment_method_nonce": nonces[0]
        }

        # 1. Do the first one Syncronously
        # Otherwise tries to create user twice
        rs = gr.post(APIADDR + 'addcredit', headers=headers, data=json.dumps(post_data)),

        responses = gr.map(rs, exception_handler=request_failed)

        self.assert_responses_are(200, responses)

        # 2. Do all the rest now that user has been created
        if len(nonces) < 2: return
        rs = []
        for nonce in nonces[1:]:
            dat = post_data.copy()
            dat['payment_method_nonce'] = nonce
            rs.append( gr.post(APIADDR + 'addcredit',
                headers=headers, data=json.dumps(dat)))

        responses = gr.map(rs, exception_handler=request_failed)
        self.assert_responses_are(200, responses)

    def nonce_fail_test(self, nonces):
        if not nonces: return

        headers = {
            'chromeauthtoken': CHROME_TOKEN,
            'Content-Type': "application/json"
        }
        post_data = {
            "credit": "1",
            "payment_method_nonce": nonces[0]
        }

        # 1. Do the first one Syncronously
        # Otherwise tries to create user twice
        rs = gr.post(APIADDR + 'addcredit', headers=headers, data=json.dumps(post_data)),

        responses = gr.map(rs, exception_handler=request_failed)

        self.assert_responses_not(200, responses)

        # 2. Do all the rest now that user has been created
        rs = []
        for nonce in nonces[1:]:
            dat = post_data.copy()
            dat['payment_method_nonce'] = nonce
            rs.append( gr.post(APIADDR + 'addcredit',
                headers=headers, data=json.dumps(dat)))

        responses = gr.map(rs, exception_handler=request_failed)
        self.assert_responses_not(200, responses)

class Test_Valid_Nonces(MyTestCase):
    def setUp(self):
        headers = {
            'chromeauthtoken': CHROME_TOKEN,
            'Content-Type': "application/json"
        }

        rs = [
            gr.delete(APIADDR + 'credit', headers=headers)
        ]
        responses = gr.map(rs, exception_handler=request_failed)
        for r in responses:
            if r.status_code == 500:
                raise Exception('Your token has probly expired')
            self.assertEqual(r.status_code, 200, msg='Endpoint failed to return success: ' + r.url)

    def test_valid_nonces(self):
        self.success_nonce_test(valid_nonces)

    def test_myvalid_nonces(self):
        self.success_nonce_test(my_valid_nonces)

class Test_InValid_Nonces(MyTestCase):
    def setUp(self):
        headers = {
            'chromeauthtoken': CHROME_TOKEN,
            'Content-Type': "application/json"
        }

        rs = [
            gr.delete(APIADDR + 'credit', headers=headers)
        ]
        responses = gr.map(rs, exception_handler=request_failed)
        for r in responses:
            if r.status_code == 500:
                raise Exception('Your token has probly expired')
            self.assertEqual(r.status_code, 200, msg='Endpoint failed to return success: ' + r.url)

    def test_processor_rejected_nonces(self):
        self.nonce_fail_test(processor_rejected_nonces)

    def test_gateway_rejected_nonces(self):
        self.nonce_fail_test(gateway_rejected_nonces)

    def test_inmyvalid_nonces(self):
        self.nonce_fail_test(my_invalid_nonces)


