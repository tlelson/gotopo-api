from __future__ import print_function

#import simplejson as json
import os
import sys
import json
import unittest2 as unittest
#import grequests as gr

os.environ["SERVERLESS_STAGE"] = "dev"

# Braintree sandbox numbers
os.environ["btree_merchant_id"] = "cjxrntyjxqpskxgy"
os.environ["btree_public_key"] = "jvj4cfv86yqgcwp2"
os.environ["btree_private_key"] = "8ff351c96193cf6badf3ccc9d07f92aa"

here = os.path.dirname(os.path.realpath(__file__))
credit_pkgs = os.path.join(here, "..", "gotopo", "functions", "credit")
sys.path.append(credit_pkgs)

import handler as credit
import braintree

print('Braintree type: {}',format(type(braintree)))

braintree.Configuration.configure(braintree.Environment.Sandbox,
                merchant_id = os.environ['btree_merchant_id'],
                public_key =  os.environ['btree_public_key'],
                private_key = os.environ['btree_private_key'])

delete_event = {
    "authorizerPrincipalId": "112535796705570131483",
    "httpMethod": "DELETE"
}

def deleteUser():
    try:
        cust_id = braintree.Customer.find(delete_event['authorizerPrincipalId'])
        result = braintree.Customer.delete(delete_event['authorizerPrincipalId'])
    except braintree.exceptions.not_found_error.NotFoundError as e:
        #print('Exception reached: ' + e.message)
        # No need to delete the user
        pass

    res = credit.handler(delete_event, None)

deleteUser()

