from __future__ import print_function

# Comes with AWS
import boto3  # not needed in package

def saveToS3(bucket, file_name, contents):
    """The bucket can't contain slashes.  Its the top level bucket. The
    rest is really just the filename """

    s3 = boto3.resource('s3', region_name='ap-southeast-2')
    client = s3.meta.client
    #client = boto3.client('s3')

    print("Uploading S3 file object")
    client.put_object(Bucket=bucket,
                ACL='public-read',
                Key=file_name,
                ContentType='text/plain',
                Body=contents)

    # Build download URL
    region = client.get_bucket_location(Bucket=bucket)['LocationConstraint']
    download_url = 'https://s3-{}.amazonaws.com/{}/{}'.format(region, bucket, file_name)

    return download_url

# Dev (Japan)
#saveToS3('gotopo.co', 'apiaddr-dev.txt', 'https://ecndr76uo9.execute-api.ap-northeast-1.amazonaws.com/dev/')

# Prod (Virginia)
saveToS3('gotopo.co', 'apiaddr-prod.txt', 'https://kff5ljwep1.execute-api.us-east-1.amazonaws.com/prod/')
