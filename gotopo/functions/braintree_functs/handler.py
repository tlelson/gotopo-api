from __future__ import print_function

import os
import json
import logging
here = os.path.dirname(os.path.realpath(__file__))

# Local imports
import braintree

log = logging.getLogger()
log.setLevel(logging.DEBUG)

# Get braintree keys (sensitive)
btree_env = os.environ['btree_env']
btree_merchant_id = os.environ['btree_merchant_id']
btree_public_key =  os.environ['btree_public_key']
btree_private_key = os.environ['btree_private_key']

def handler(event, context):
    log.debug("Received event {}".format(json.dumps(event)))
    log.debug("Braintree Environment: {}".format(btree_env))

    if btree_env == 'Sandbox':
        btenv = braintree.Environment.Sandbox
    elif btree_env == 'Production':
        btenv = braintree.Environment.Production
    else:
        raise Exception('Invalid braintree environment {}'.format(btree_env))

    braintree.Configuration.configure(btenv,
            merchant_id=btree_merchant_id,
            public_key=btree_public_key,
            private_key=btree_private_key)

    googleid = str(event['authorizerPrincipalId'])

    try:
        # 1. Try to find existing customer
        braintree.Customer.find(googleid)

        client_token = braintree.ClientToken.generate({
            "customer_id": googleid
        })
    except braintree.exceptions.not_found_error.NotFoundError as e:
        # 2. deal with new customer
        client_token = braintree.ClientToken.generate()

    return client_token

if __name__ == '__main__':
    with open(os.path.join(here, 'event.json'), 'r') as f:
        data = json.load(f)
        print(handler(data, None))
