# DOES SHIT THAT SERVERLESS SHOULD DO

from __future__ import print_function

# Comes with AWS
import logging
import io
import sys
import os
here = os.path.dirname(os.path.realpath(__file__))

# Import installed packages (in site-packages)
site_pkgs = os.path.join(here, "..", "site-packages")
sys.path.append(site_pkgs)
import simplejson as json

# Append an environment variable
os.environ['TEST_VAR'] = "{}:{}".format('flojo', os.environ.get('TEST_VAR',''))

# Symlink my linked libraries to /var/task/lib because we can't add
# to LD_LIBRARY_PATH in time
#src = os.path.join(os.getcwd(), 'lib', 'libmkl_intel_lp64.so')
#dst = os.path.join(os.getcwd(), "libmkl_intel_lp64.so")
#print("symlinking: {} to {}".format(src, dst))

from handler_real import handler as hdl

def handler(event, context):
    return hdl(event, context)

if __name__ == '__main__':
    with open(os.path.join(here, 'event.json'), 'r') as f:
        data = json.load(f)
        print(handler(data, None))
