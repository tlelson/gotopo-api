from __future__ import print_function

# Comes with AWS
import boto3  # not needed in package
import logging
import io
import sys
import os
here = os.path.dirname(os.path.realpath(__file__))
import string
import random
import datetime as dt
import urllib

# Check environment variables are set
print("Current working directory: {}".format(os.getcwd()))
print("... contains files: {}".format(os.listdir(".")))
for key in os.environ.keys():
        print("{} - {}".format(key, os.environ[key]))

# Import installed packages (in site-packages)
site_pkgs = os.path.join(here, "..", "site-packages")
sys.path.append(site_pkgs)
import simplejson as json
import urllib2
import zipfile

# Import homemade packages (project_utils)
sys.path.append(os.path.join(here, "..", "project_utils"))
from utils import fail_if_not_connected, get_token_data
from credit_utils import remove_credit, get_credit

# Import tmapper
for f in os.listdir(site_pkgs):
    if f[:7] == 'tmapper':
        tmapper_egg = f
sys.path.append(os.path.join(site_pkgs, tmapper_egg))
from tmapper import TMap

log = logging.getLogger()
log.setLevel(logging.DEBUG)

# Constants
BUCKET = 'gotopo'
TEST_MAPID_NO_URL = "1jMux5ghyPMDXEQrSDFb7rLy8SZ0"


def get_date_string():
    today = dt.date.today()
    return today.strftime("%Y%m%d")

def get_random_string(size=20, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def cleanS3(date):
    """ Remove all buckets before some date
    """
    pass

def saveToS3(data, filename):
    """The bucket can't contain slashes.  Its the top level bucket. The
    rest is really just the filename """

    s3 = boto3.resource('s3')
    client = s3.meta.client
    #client = boto3.client('s3')

    date = get_date_string()
    file_name = 'maps/' + date + '/' + get_random_string() + '/' + urllib.quote_plus(filename)

    log.debug("Uploading S3 file object")
    client.put_object(Bucket=BUCKET,
                ACL='public-read',
                Key=file_name,
                Body=data)

    # Build download URL
    region = client.get_bucket_location(Bucket=BUCKET)['LocationConstraint']
    download_url = 'https://s3-{}.amazonaws.com/{}/{}'.format(region, BUCKET, file_name)

    return download_url

def handler(event, context):
    log.debug("Received event {}".format(json.dumps(event)))

    # 1. get mapid
    try:
        mapid = event['mid']
    except KeyError:
        raise Exception("BadRequest: is Content-Type 'application/json' ?")

    # 0. Get user details
    googleid = event['authorizerPrincipalId']

    #1a. Check user has available credits
    map_credits = get_credit(googleid)['map_credits']
    if map_credits < 1:
        raise Exception('Unauthorized: Insufficient map credits available to generate this map')

    # 2. download KMZ into memory
    request = urllib2.Request('https://www.google.com/maps/d/kml?mid={}'.format(mapid))
    log.debug(request.get_full_url())
    try:
        response = urllib2.urlopen(request)
    except urllib2.HTTPError:
        raise Exception("MapNotFound: Map not shared to those with link")

    headers = response.info().headers
    data = response.read()
    z = zipfile.ZipFile(io.BytesIO(data))
    kml_data = z.read('doc.kml')

    # 3a. Return test cases
    if mapid == TEST_MAPID_NO_URL:
        return {'mid': mapid, 'message': 'Testing map used. Response 200 with no download_url' }

    # 3b. create map

    # Create a new tmap
    tmap = TMap(loglevel='INFO')

    tmap.load_waypoints_from_xml_string(kml_data)

    # 1. Look at output before legend
    tmap.build_image_async()
    tmap.mark_all_waypoints()
    tmap.add_grid('utm')
    tmap.add_dms_border()
    #tmap.set_title("YoYOO")
    tmap.add_legend()

    #tmap.show_map()

    # 4. save map gif to s3
    imgfile = io.BytesIO()
    tmap.modified_map_image.save(imgfile, format='JPEG')
    imagestring = imgfile.getvalue()
    download_url = saveToS3(imagestring, tmap.map_name.replace(' ', '_') + '.jpeg')

    # 5. Reduce user credit
    res = remove_credit(googleid, 1)  # add negative credit
    # This is not supposed to reach this point if the user has no map credits but
    # just in case ...
    if res['map_credits'] < 0:
        raise Exception('Unauthorized: No map credits')

    return {"download_url": download_url, 'mid': mapid,
             'message': 'Successfully retreived map' }

