from __future__ import print_function

# Comes with AWS
import boto3  # not needed in package
from boto3.dynamodb.conditions import Key, Attr
import logging
import sys
import os
here = os.path.dirname(os.path.realpath(__file__))

# Import installed packages (in site-packages)
sys.path.append(os.path.join(here, "..", "site-packages"))
import simplejson as json

# Import homemade packages (project_utils)
sys.path.append(os.path.join(here, "..", "project_utils"))
from policygen import AuthPolicy, HttpVerb
from credit_utils import get_credit
from utils import fail_if_not_connected, register_user, get_token_data, get_user_data

log = logging.getLogger()
log.setLevel(logging.DEBUG)

CHROME_EXT_CLIENT_ID = "864719189874-9hjohbcan5fqbhgoh5e7u26t37cpp681.apps.googleusercontent.com"

def determine_patch_policy(methodArn, usergoogleID):
    """Especially for the tempory requirements to authenticate credit updates.
    - Uses GoTopo-guser
    - Only requires the user exists
    """

    # Get some clear variables for policy generation
    tmp = methodArn.split(':')
    apiGatewayArnTmp = tmp[5].split('/')
    awsAccountId = tmp[4]
    policy = AuthPolicy(usergoogleID, awsAccountId)
    policy.restApiId = apiGatewayArnTmp[0]
    policy.region = tmp[3]
    policy.stage = apiGatewayArnTmp[1]
    policy.allowAllMethods()

    policy_doc = policy.build()

    return policy_doc

def valid_auth_token_response(data):
    """Returns: Boolean
    """

    # Ensure the token has time left on it
    has_time = data.get('expires_in', None) > 0
    # Ensure the clientid that requested the token is one of ours!
    our_clientid = data.get('aud', None) == CHROME_EXT_CLIENT_ID

    if has_time and our_clientid:
        return True

    return False

def handler(event, context):
    print(event)
    print('Client token: ' + event['authorizationToken'])
    print('Method ARN: ' + event['methodArn'])

    idinfo = get_token_data(event['authorizationToken'])

    # 1. Check Token
    if not valid_auth_token_response(idinfo):
        raise Exception("Invalid Auth Token")

    # 2. If user is valid decide on a policy
    policy_doc = determine_patch_policy(event['methodArn'], idinfo['sub'])

    # 3.  Get user google data
    userinfo = get_user_data(event['authorizationToken'])
    register_user(userinfo)

    return policy_doc

if __name__ == '__main__':
    with open('event.json', 'r') as f:
        data = json.load(f)
        print(handler(data, None))
