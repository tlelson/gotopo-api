from __future__ import print_function
import time

import boto3
from boto3.dynamodb.conditions import Key, Attr

# Import local packages
import sys
import os

import simplejson as json
import urllib

def fail_if_not_connected(res):
    # If the query doesn't work deal with it
    if res['ResponseMetadata']['HTTPStatusCode'] != 200:
        print("DynamoDB operation Error")
        raise Exception("DynamoDB operation Error")
    return res

def register_user(userinfo):
    stage = os.environ['SERVERLESS_STAGE']
    dynamo = boto3.resource('dynamodb')
    tbl = dynamo.Table('GoTopo-User-{}'.format(stage))  # Can be created automatically

    # if the user exists, add it
    res = fail_if_not_connected(tbl.query(
        KeyConditionExpression=Key('googleid').eq(userinfo['sub']),
        Select='ALL_ATTRIBUTES'
        #Select='COUNT'
    ))

    print(res)

    # Assuming we got a 200 response from our query
    if res['Count']:
        # Possibly check userinfo is the same, and save if not
        print("User exists")

        if res['Items'][0]['user_info'][0]['info'] != userinfo:
			print('User details have changed, saving ...')
			fail_if_not_connected(tbl.update_item(
					Key={
						"googleid": userinfo['sub']
					},
					UpdateExpression="SET user_info = list_append(:i, user_info)",
					ExpressionAttributeValues={
						':i': [{
							'created': str(int(time.time())),
							'info': userinfo
						}]
					},
					ReturnValues="UPDATED_NEW"
				)
			)
    else:
        print("New user.  Adding ...")
        fail_if_not_connected(
            tbl.put_item(
                Item={
                    "googleid": userinfo['sub'],
                    "user_info" : [{
                        'created': str(int(time.time())),
                        'info': userinfo
                    }],
                }
            )
        )

    return

def get_token_data(token):
    url = "https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=" + token
    return json.loads(urllib.urlopen(url).read())

def get_user_data(token):
    url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=" + token
    return json.loads(urllib.urlopen(url).read())


