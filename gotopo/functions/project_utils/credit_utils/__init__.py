from __future__ import print_function
import sys
import os
import boto3
from boto3.dynamodb.conditions import Key, Attr
from decimal import Decimal
from utils import fail_if_not_connected, get_token_data

def _get_user_tbl():
    """Gets the DynamoDB table object for utility functions"""

    stage = os.environ['SERVERLESS_STAGE']

    dynamo = boto3.resource('dynamodb')
    user_tbl = dynamo.Table('GoTopo-MapCredits-{}'.format(stage))
    return user_tbl

def get_credit(googleid, **kwargs):
    """ kwargs not used yet """

    user_tbl = _get_user_tbl()

    # Now that the token is valid, lets check if the user has credits
    res = fail_if_not_connected(user_tbl.query(
        KeyConditionExpression=Key('id').eq(googleid)
    ))

    if res['Count'] < 1:
        map_credits = None
    else:
        map_credits = res['Items'][0]['map_credits']

    return {'map_credits': map_credits}

def add_credit(googleid, new_credits):
    user_tbl = _get_user_tbl()
    map_credits = None
    print('Attempting to add credit ...')

    # 1. Check if user exists already
    res = fail_if_not_connected(user_tbl.query(
        KeyConditionExpression=Key('id').eq(googleid)
    ))

    if res['Count'] > 0:
        print('User found in table. Update their credits')
        res = fail_if_not_connected( user_tbl.update_item(
            Key={
                'id': googleid
            },
            UpdateExpression = "SET map_credits = map_credits + :val1",
            ExpressionAttributeValues = { ":val1" : new_credits },
            ReturnValues="UPDATED_NEW"
        ))
        map_credits = res['Attributes']['map_credits']
    else:
        print('User not in table yet. Add them and their credits')
        fail_if_not_connected(user_tbl.put_item(
            Item={
                'id': googleid,
                'map_credits': Decimal(str(new_credits))
            },
            ReturnValues="ALL_OLD"
        ))
        map_credits = new_credits

    print('Map credits: ', map_credits)
    return {'map_credits': map_credits}

def remove_credit(googleid, credits):
    user_tbl = _get_user_tbl()
    map_credits = None
    print('attempting to remove credit ...')

    # 1. Check if user exists already
    res = fail_if_not_connected(user_tbl.query(
        KeyConditionExpression=Key('id').eq(googleid)
    ))

    if res['Count'] > 0:
        print('User found in table. Update their credits')
        res = fail_if_not_connected( user_tbl.update_item(
            Key={
                'id': googleid
            },
            UpdateExpression = "SET map_credits = map_credits + :val1",
            ExpressionAttributeValues = { ":val1" : -1*credits },
            ReturnValues="UPDATED_NEW"
        ))
        map_credits = res['Attributes']['map_credits']

    print('Map credits: ', map_credits)
    return {'map_credits': map_credits}

def set_credit(googleid, new_credits):

    user_tbl = _get_user_tbl()
    map_credits = None
    # 1. Check if user exists already
    res = fail_if_not_connected(user_tbl.query(
        KeyConditionExpression=Key('id').eq(googleid)
    ))

    if res['Count'] > 0:
        print('User found in table. Update their credits')
        res = fail_if_not_connected( user_tbl.update_item(
            Key={
                'id': googleid
            },
            UpdateExpression = "SET map_credits = :val1",
            ExpressionAttributeValues = { ":val1" : new_credits },
            ReturnValues="UPDATED_NEW"
        ))
        map_credits = res['Attributes']['map_credits']
    else:
        print('User not in table yet. Add them and their credits')
        fail_if_not_connected(user_tbl.put_item(
            Item={
                'id': googleid,
                'map_credits': Decimal(str(new_credits))
            },
            ReturnValues="ALL_OLD"
        ))
        map_credits = new_credits

    print('Map credits: ', map_credits)
    return {'map_credits': map_credits}

def delete_credit_user(googleid):
    user_tbl = _get_user_tbl()
    res = fail_if_not_connected(user_tbl.delete_item(
        Key={
            'id': googleid
        }
    ))

    return {"code": 0, "message": "User deleted"}


