from __future__ import print_function

# Come with AWS
import logging

# Import local packages
import sys
import os

import braintree

print('Braintree type: {}',format(type(braintree)))

here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "..", "site-packages"))
sys.path.append(os.path.join(here, "..", "project_utils"))

import simplejson as json
from credit_utils import get_credit, add_credit, set_credit, delete_credit_user
from utils import get_token_data

log = logging.getLogger()
log.setLevel(logging.DEBUG)

btree_env = os.environ['btree_env']
print("Braintree Environment: {}".format(btree_env))

if btree_env == 'Sandbox':
    btenv = braintree.Environment.Sandbox
elif btree_env == 'Production':
    btenv = braintree.Environment.Production
else:
    raise Exception('Invalid braintree environment {}'.format(btree_env))

braintree.Configuration.configure(btenv,
        merchant_id = os.environ['btree_merchant_id'],
        public_key =  os.environ['btree_public_key'],
        private_key = os.environ['btree_private_key'])

class MyException(Exception):
    pass

def do_payment(clientid, amount):
    result = braintree.Transaction.sale({
        "amount": amount,
        "customer_id": clientid,
        "options": {
            "submit_for_settlement": True
        }
    })

    print('Transaction results: ' + str(result.is_success))

    # Validation
    if not result.is_success:
        print(result.message)
        raise MyException("PaymentFailed: " + result.message)

    print('Succesful payment with: ')
    print(result.transaction.credit_card)
    print(result.transaction.processor_response_text)

def get_coupon_credits(coup_code):
    return 10 if coup_code == "ERN-7HJ" else None

def manage_braintree_customer(clientid, nonce):
    options = {
        "make_default": True,
        "verify_card": True,
    }

    # 1.    Check if client exists
    try:
        cust_id = braintree.Customer.find(clientid)
        print('Existing braintree client found for {}: {}'.format(clientid, cust_id))

        # 2b.  If customer exists, update payment method
        # if its the same then it passes fine
        result = braintree.PaymentMethod.create({
            "customer_id": clientid,
            "payment_method_nonce": nonce,
            "options": options,
        })

    except braintree.exceptions.not_found_error.NotFoundError as e:
        print('No braintree client found for {}'.format(clientid))

        # 2a.  If customer doesn't exist
        result = braintree.Customer.create({
            "id": clientid,
            "credit_card": {
                "payment_method_nonce": nonce,
                "options": options,
            }
        })

    # 3.    Check payment method is valid
    if not result.is_success:
        print(result.message)
        raise MyException("PaymentMethodInvalid: " + result.message)

    print('Succesful payment method saved ... ')

def handler(event, context):
    print("Received event {}".format(json.dumps(event)))

    # 0. Get user details
    # If the wrong Content-Type is used, the data is passed straight
    # through.  We really should check for all fields but it would
    # be messy and I expect we are able to configure
    # 'Request body passthrough' == 'Never' soon
    # ...for now: This could be fucked with by passing data with
    # the wrong Content-Type if one field is 'authorizerPrincipalId'
    try:
        googleid = event['authorizerPrincipalId']
    except KeyError:
        raise MyException("BadRequest: is Content-Type 'application/json' ?")

    message = ''
    error_message = ''
    # 1. check event['httpMethod'] to determine which method to use
    # TODO: All this logic should be in the serverless.yml in v1.0
    if event['httpMethod'] == 'GET':
        operation = get_credit
        kwargs = {}
        if '{coupon}' in event['resourcePath'].split('/'):
            coupon_credits = get_coupon_credits(event.get('coupon'))
            if coupon_credits:
                operation = set_credit
                kwargs = {'new_credits': coupon_credits}
                message = 'coupon processed successfully!'
            else:
                error_message = 'invalid coupon'
    elif event['httpMethod'] == 'POST':
        if (not (event.get('payment_nonce') or event.get('credits'))) and event.get('data'):
                raise MyException("Data not received in JSON")

        nonce = event['payment_nonce']

        # If the user has changed payment methods multiple nonces
        # will be sent.  The last one is the one to use.
        if isinstance(nonce, basestring):
            nonce = nonce
        elif isinstance(nonce, list):
            nonce = nonce[-1]
        else:
            raise MyException('Payment nonce invalid')

        # These Error out if fail
        manage_braintree_customer(googleid, nonce)
        do_payment(googleid, event['credits'])

        # Payment has succeeded
        operation = add_credit
        kwargs = {'new_credits': int(float(event['credits']))}

    elif event['httpMethod'] == 'DELETE':
        operation = delete_credit_user
        kwargs = {}
    else:
        raise MyException('BadMethod: HTTP method not allowed')

    res = operation(googleid, **kwargs)
    if message: res['message'] = message
    if error_message: res['error_message'] = error_message
    return res

if __name__ == '__main__':
    # Basic event test
    with open('eventPOST.json', 'r') as f:
        data = json.load(f)
        print(handler(data, None))



